import { Container } from 'react-bootstrap';
import './App.css';
import { UserAuthContext } from './Contexts/UserAuthContext';
import React from 'react';
import Layout from './Components/Layout/Layout';
import { IntlProvider } from "react-intl";
import Messages from './Resources/locale/language.json';
import Loader from './Components/Loader/Loader';


function App() {
  const [UserAuthData] = React.useContext(UserAuthContext);
  const [isLoading] = React.useState(false);
  const language = (navigator.language.substring(0, 2) !== "en" && navigator.language.substring(0, 2) !== "es") ?
    'en' :
    navigator.language.substring(0, 2);

  return (
    <Container fluid>
      {
        isLoading ?? <Loader />
      }
      <IntlProvider messages={Messages[language]} locale={language}>
        <UserAuthContext.Provider value={UserAuthData}>
          <Layout user={UserAuthData} />
        </UserAuthContext.Provider>
      </IntlProvider>
    </Container>
  );
}

export default App;
