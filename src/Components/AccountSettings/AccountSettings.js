import React from "react";
import { Form } from "react-bootstrap";
import { UserAuthContext } from "../../Contexts/UserAuthContext";
import { FormattedMessage } from 'react-intl/';

function AccountSettings() {
    const user = React.useContext(UserAuthContext);
    return (
        <>
            <Form>
                <Form.Group>
                    <Form.Label>
                        <FormattedMessage id={'App.account.labelname'} />
                    </Form.Label>
                    <Form.Control type="text" placeholder={user.name} disabled />
                </Form.Group>
                <Form.Group>
                    <Form.Label>
                        <FormattedMessage id={'App.account.mail'} />
                    </Form.Label>
                    <Form.Control type="text" placeholder={user.username} disabled />
                </Form.Group>
            </Form>
        </>
    )
}

export default AccountSettings;