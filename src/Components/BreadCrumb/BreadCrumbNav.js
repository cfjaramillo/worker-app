import React from "react";
import { Breadcrumb, Navbar } from "react-bootstrap";

function BreadCrumbNav() {

    const paths = window.location.pathname.split('/');

    function buildPath(route) {
        let path = ''

        paths.forEach(value => {
            path.concat(`/${value}`)
            if (value === route) {
                return path
            }
        });
    }

    function GetPath(path) {
        const newLocation = buildPath(path)
        window.location.href = newLocation
    }

    return (
        <Navbar bg={'dark'}>
            <Breadcrumb>
                {
                    paths && paths.map(
                        (path, index) => {
                            return (
                                <Breadcrumb.Item key={index} onClick={() => { GetPath(path) }}>
                                    {path}
                                </Breadcrumb.Item>
                            )
                        }
                    )
                }
            </Breadcrumb>
        </Navbar>
    )
}

export default BreadCrumbNav;