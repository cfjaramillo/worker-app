import Card from "react-bootstrap/Card";
import { FormattedMessage } from "react-intl";

function Error() {
    return (
        <Card>
            <Card.Header>
                <Card.Title>
                    <FormattedMessage id={'App.Error.Title'} />
                </Card.Title>
            </Card.Header>
            <Card.Body>
                <Card.Text>
                    <FormattedMessage id={'App.Error.Body'} />
                </Card.Text>
            </Card.Body>
        </Card>
    )
}

export default Error;