import React from "react";
import { Alert, Badge } from "react-bootstrap";
import { GetRequest } from "../../Utils/AxiosClient";
import Loader from "../Loader/Loader";
import { FormattedMessage } from 'react-intl/';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import './Home.css';
import JobImage from '../../Resources/img/jobs.jpg';
import { Link } from "react-router-dom";
import { GetStatusColor } from "../../Utils/Utils";
import { CatalogTypes } from "../../Resources/Constants/CONSTANTS";

function Home() {

    const [isLoading, setLoading] = React.useState(true);
    const [data, setData] = React.useState([]);
    const [jobTypes, setJobTypes] = React.useState([]);
    const [filter, setFilter] = React.useState([]);
    const [filterId, setFilterId] = React.useState(0);

    React.useEffect(
        () => {
            function fetchData() {
                GetRequest('Job').then(
                    (res) => {
                        setLoading(false);
                        setData(res.data);
                        setFilter(res.data);
                        console.log(res);
                    }
                ).catch(
                    (err) => {
                        setLoading(false);
                        console.log(err);
                    }
                );
            }
            function fetchJobTypes() {
                GetRequest(`Catalog/catalogtype/${CatalogTypes.WorkCategories}`).then(
                    (res) => {
                        setLoading(false);
                        setJobTypes(res.data);
                        console.log(res);
                    }
                ).catch(
                    (err) => {
                        setLoading(false);
                        console.log(err);
                    }
                );
            }
            fetchData();
            fetchJobTypes();
        }, [isLoading]
    )

    const filterData = (id) => {
        if (id === filterId) {
            setFilter(data);
            setFilterId(0);
        }
        else {
            const result = data.filter(o => o.jobTypeCatalogsId === id);
            setFilter(result);
            setFilterId(id);
        }
    }

    return (
        <>
            {
                isLoading ? <Loader /> : null
            }
            <div className="page-title">
                <h1>
                    <FormattedMessage id={'App.home.Label'} />
                </h1>
            </div>
            {
                jobTypes.map(
                    (item, index) => {
                        return (
                            <Badge
                                key={index}
                                bg={filterId === item.id ? 'primary' : 'light'}
                                text={'dark'}
                                onClick={() => { filterData(item.id) }}>
                                {item.name}
                            </Badge>
                        )
                    }
                )
            }
            <div className="flex-container">
                {
                    filter.length > 0 ? filter.map(
                        (item) => {
                            return (
                                <Card className="flex-item" key={item.id}>
                                    <Card.Header>
                                        <p>
                                            <Badge bg={GetStatusColor(item.statusCatalogs?.id)}>{item.statusCatalogs?.name}</Badge>
                                        </p>
                                        <div className="scroll-name">
                                            <strong>{item.name}</strong>
                                        </div>
                                    </Card.Header>
                                    <Card.Img className="card-image" src={JobImage} alt="image" />
                                    <Card.Body>
                                        <p className="scroll-jobtype">
                                            {item.jobTypeCatalogs?.name}
                                        </p>
                                        <Card.Text className="scroll-description">
                                            {item.description}
                                        </Card.Text>

                                        <Button variant="primary">
                                            <Link to={`/jobs/${item.id}`}>
                                                <FormattedMessage id={'Job.Card.Button.Visit'} />
                                            </Link>
                                        </Button>
                                    </Card.Body>
                                </Card>
                            )
                        }
                    ) :
                        <div style={{ 'width': '100%' }}>
                            <Alert variant="primary">
                                <Alert.Heading>
                                    <FormattedMessage id={'App.home.Empty.Header'} />
                                </Alert.Heading>
                                <p>
                                    <FormattedMessage id={'App.home.Empty.Body'} />
                                </p>
                            </Alert>
                        </div>
                }
            </div>
        </>
    )
}

export default Home;