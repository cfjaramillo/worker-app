import React from "react";
import { Badge, Card, Col, Container, Row } from "react-bootstrap";
import { useParams } from "react-router-dom";
import { GetRequest } from "../../Utils/AxiosClient";
import Loader from "../Loader/Loader";
import Jobs from '../../Resources/img/jobs.jpg';

import { GetStatusColor } from "../../Utils/Utils";
import './JobDetails.css';

function JobDetails() {

    const [job, setJob] = React.useState();
    const [isLoading, setLoading] = React.useState(true);
    const params = useParams();

    React.useEffect(
        () => {

            function fetchData() {
                GetRequest(`Job/${params.id}`).then(
                    (res) => {
                        setLoading(false);
                        setJob(res.data);
                        console.log(res);
                    }
                ).catch(
                    (err) => {
                        setLoading(false);
                        console.log(err);
                    }
                );
            }
            fetchData();
        }, [isLoading, params.id]);

    return (
        <>
            {
                isLoading ? <Loader /> : null
            }
            <Card>
                <Container>
                    <Row>
                        <Col>
                            <Row>
                                
                            </Row>
                            <Row>
                                <Card.Img src={Jobs}/>
                            </Row>
                        </Col>
                        <Col>
                            <Row>
                                <Card.Title className="job-title">
                                    {job?.name}<Badge bg={GetStatusColor(job?.statusCatalogs?.id)}>{job?.statusCatalogs?.name}</Badge>
                                </Card.Title>
                            </Row>
                            <Row>
                                <Card.Text className="job-description">
                                    {job?.description}
                                </Card.Text>
                            </Row>
                        </Col>
                    </Row>
                </Container>
            </Card>
        </>
    )
}

export default JobDetails;