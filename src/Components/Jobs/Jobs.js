import Button from 'react-bootstrap/Button';
import React from "react";
import Modal from 'react-bootstrap/Modal';
import Form from 'react-bootstrap/Form';
import { FormattedMessage } from 'react-intl/';
import { GetRequest, PostRequest } from '../../Utils/AxiosClient';
import Loader from '../Loader/Loader';
import { CatalogTypes, StatusTypes } from '../../Resources/Constants/CONSTANTS';
import { useFormik } from 'formik';
import ToastMessages from '../Toast/ToastMessages';
import { account } from '../../Utils/MsalClient';
import { BarChart, Bar, XAxis, YAxis, Tooltip } from 'recharts';

function Jobs() {

    const [modal, setModal] = React.useState(false);
    const [data, setData] = React.useState([]);
    const [isLoading, setLoading] = React.useState(true);
    const [toastMessage, setToastMessage] = React.useState([]);
    const [jobTypes, setJobTypes] = React.useState([]);
    const [chartData, setChartData] = React.useState([]);
    const UserAuthData = account;
    const formik = useFormik({
        initialValues: {
            jobname: '',
            jobdescription: '',
            jobtypecatalog: ''
        },
        onSubmit: values => {
            setLoading(true);
            console.log(values);
            CreateWork();
        },
        validate: values => {
            let errors = {};

            if (!values.jobname) {
                errors.jobname = '*'
            }

            if (!values.jobdescription) {
                errors.jobdescription = '*'
            }

            if (!values.jobtypecatalog) {
                errors.jobtypecatalog = '*'
            }

            return errors;
        }
    });



    async function CreateWork() {
        const newWork = {
            name: formik.values.jobname,
            description: formik.values.jobdescription,
            jobTypeCatalogsId: formik.values.jobtypecatalog,
            statusCatalogsId: StatusTypes.Created
        }

        PostRequest(`Job`, newWork).then(
            (res) => {
                PostRequest('Email', {
                    emailTo: UserAuthData.username,
                    nameTo: UserAuthData.name,
                    subject: 'Worker APP Notifications',
                    plainContent: 'You Job has been succesfully created!'
                }).then(
                    (res) => {
                        setLoading(false);
                        setModal(false);
                        setToastMessage(["Job Created!"])
                    }
                ).catch(
                    (err) => {
                        setToastMessage(["Notification error!"])
                    }
                )
            }
        ).catch(
            (err) => {
                setLoading(false);
                console.log(err);
            }
        );

    }

    React.useEffect(
        () => {
            function fetchData() {
                GetRequest(`Catalog/catalogtype/${CatalogTypes.WorkCategories}`).then(
                    (res) => {
                        setLoading(false);
                        setJobTypes(res.data);
                        console.log(res);
                    }
                ).catch(
                    (err) => {
                        setLoading(false);
                        console.log(err);
                    }
                );
            }
            function fetchJobs() {
                GetRequest(`Job/`).then(
                    (res) => {
                        setLoading(false);
                        setData(res.data);
                        console.log(res);
                    }
                ).catch(
                    (err) => {
                        setLoading(false);
                        console.log(err);
                    }
                );
            }
            function fetchChart() {
                let chart = [];
                jobTypes.forEach(element => {
                    chart.push({
                        Categoria: element.name, Cantidad: data.filter(o => o.jobTypeCatalogsId === element.id).length
                    })
                });
                setChartData(chart);
            }
            fetchData();
            fetchJobs();
            fetchChart();
        }, [isLoading, data, jobTypes]);

    return (
        <>
            {
                isLoading ? <Loader /> : null
            }
            {
                toastMessage ? toastMessage.map(
                    (message) => {
                        return (
                            <ToastMessages key={message}>
                                {message}
                            </ToastMessages>
                        )
                    }
                ) : null
            }
            <div className="page-title">
                <h1>
                    <FormattedMessage id={'App.Jobs.Label'} />
                </h1>
            </div>
            <Button variant="primary" onClick={() => { setModal(true) }}>
                <FormattedMessage id={'App.Job.Create.Button'} />
            </Button>
            {
                <div style={{ 'display': 'flex', 'justifyContent': 'center' }}>
                    <BarChart
                        width={500}
                        height={300}
                        data={chartData}>
                        <XAxis dataKey={'Categoria'} />
                        <YAxis dataKey={'Cantidad'} />
                        <Tooltip />
                        <Bar dataKey={'Cantidad'} fill={'black'} />
                    </BarChart>
                </div>
            }
            <Modal show={modal}>
                <Form onSubmit={formik.handleSubmit}>
                    <Modal.Header>
                        <Modal.Title>
                            <FormattedMessage id={'App.Job.Modal.Title'} />
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>

                        {/* Name */}
                        <Form.Group className="mb-3" controlId="jobname">
                            <Form.Label>
                                <FormattedMessage id={'Job.Form.Name.Label'} />
                                <span className='error'>
                                    {formik.errors.jobname && <span>*</span>}
                                </span>
                            </Form.Label>
                            <Form.Control isInvalid={formik.errors.jobname} type="text" onChange={formik.handleChange} value={formik.values.jobname} />
                            <Form.Text className="text-muted">
                                <FormattedMessage id={'Job.Form.Name.Placeholder'} />
                            </Form.Text>
                            {
                                formik.errors.jobname &&
                                <Form.Control.Feedback type="invalid">
                                    <FormattedMessage id={'Job.Form.Name.Error'} />
                                </Form.Control.Feedback>
                            }
                        </Form.Group>

                        {/* Description */}
                        <Form.Group className="mb-3" controlId="jobdescription">
                            <Form.Label>
                                <FormattedMessage id={'Job.Form.Description.Label'} />
                                <span className='error'>
                                    {formik.errors.jobdescription && <span>*</span>}
                                </span>
                            </Form.Label>
                            <Form.Control as={'textarea'} isInvalid={formik.errors.jobdescription} type="text" onChange={formik.handleChange} value={formik.values.jobdescription} />
                            <Form.Text className="text-muted">
                                <FormattedMessage id={'Job.Form.Name.Placeholder'} />
                            </Form.Text>
                            {
                                formik.errors.jobdescription &&
                                <Form.Control.Feedback type="invalid">
                                    <FormattedMessage id={'Job.Form.Description.Error'} />
                                </Form.Control.Feedback>
                            }
                        </Form.Group>

                        {/* JobType */}
                        <Form.Group className="mb-3" controlId="jobtypecatalog">
                            <Form.Label>
                                <FormattedMessage id={'Job.Form.JobType.Label'} />
                                <span className='error'>
                                    {formik.errors.jobtypecatalog && <span>*</span>}
                                </span>
                            </Form.Label>
                            <Form.Select onChange={formik.handleChange} value={formik.values.jobtypecatalog} isInvalid={formik.errors.jobtypecatalog}>
                                <option key={0} value={''}>
                                    <FormattedMessage id={'App.Form.Select.Default'} />
                                </option>
                                {
                                    jobTypes.map(
                                        (item) => {
                                            return (
                                                <option key={item.id} value={item.id}>
                                                    {item.name}
                                                </option>
                                            )
                                        }
                                    )
                                }
                            </Form.Select>
                            {
                                formik.errors.jobtypecatalog &&
                                <Form.Control.Feedback type="invalid">
                                    <FormattedMessage id={'Job.Form.JobType.Error'} />
                                </Form.Control.Feedback>
                            }
                        </Form.Group>



                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={() => { setModal(false) }}>
                            <FormattedMessage id={'Job.Form.Cancel'} />
                        </Button>
                        <Button variant="primary" type='submit'
                            disabled={formik.values.jobdescription === '' || formik.values.jobname === '' || formik.values.jobtypecatalog === ''}>
                            <FormattedMessage id={'Job.Form.Submit'} />
                        </Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        </>
    )
}

export default Jobs;