import { BrowserRouter, Routes, Route } from "react-router-dom";
import Login from '../Login/Login';
import Home from '../Home/Home';
import Menu from "../Menu/Menu";
import Requests from "../Requests/Requests";
import Offers from "../Offers/Offers";
import Jobs from "../Jobs/Jobs";
import Logout from "../Logout/Logout";
import AccountSettings from "../AccountSettings/AccountSettings";
import JobDetails from "../Jobs/JobDetails";
import Error from "../Error/Error";

function Layout({ user }) {

    return (
        <BrowserRouter>
            {user === undefined ?
                <>
                    <Routes>
                        <Route path='/' element={<Login />} />
                    </Routes>
                </>
                :
                <>
                    <Menu />
                    <Routes>
                        <Route path="/" element={
                            <>
                                <Home />
                            </>

                        } />
                        <Route path="/jobs" element={
                            <>
                                <Jobs />
                            </>
                        } />
                        <Route path="/jobs/:id" element={
                            <>
                                <JobDetails />
                            </>
                        } />
                        <Route path="/offers" element={
                            <>
                                <Offers />
                            </>
                        } />
                        <Route path="/requests" element={
                            <>
                                <Requests />
                            </>
                        } />
                        <Route path="/account" element={
                            <>
                                <AccountSettings />
                            </>
                        } />
                        <Route path="/logout" element={
                            <>
                                <Logout />
                            </>
                        } />
                        <Route path="*" element={<Error />} />
                    </Routes>
                </>
            }
        </BrowserRouter>
    )
}

export default Layout;