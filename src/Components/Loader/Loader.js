import { Spinner } from "react-bootstrap";

function Loader() {
    return (
        <div className="loader">
            <Spinner animation="border" role="status" />
        </div>
    )
}

export default Loader;