import Button from 'react-bootstrap/Button';
import Stack from 'react-bootstrap/Stack';
import Card from 'react-bootstrap/Card';
// import Logo from '../../Resources/img/logo.svg';
import Jobs from '../../Resources/img/jobs.jpg';
import './Login.css';
import React from 'react';
import { msalInstance } from '../../Utils/MsalClient';
import { FormattedMessage } from 'react-intl/';
import Recaptcha from '../../Utils/Recaptcha';
import Loader from '../Loader/Loader';

function Login() {

    const [isLoading, setLoading] = React.useState(false);

    function LoginUser() {
        setLoading(true);
        msalInstance.loginPopup()
            .then((res) => {
                msalInstance.acquireTokenSilent({
                    scopes: process.env.REACT_APP_ADD_WEBAPP_SCOPES.split(","),
                    account: msalInstance.getAllAccounts()[0],
                }).then(
                    (res) => {
                        setLoading(false);
                        sessionStorage.setItem('workertoken', res?.accessToken);
                        window.location.href = "/";
                    }
                ).catch(
                    (err) => {
                        setLoading(false);
                        console.log(err);
                    }
                )
            })
            .catch((err) => { setLoading(false); console.log(err); })
    }


    return (
        <div className='loginCard'>
            {
                isLoading ? <Loader /> : null
            }

            <Card className="bg-dark text-white">
                <Card.Img variant={'top'} className="imageLogo" src={Jobs} alt={'logo'} />
                <Card.Title className='centerText'>
                    <FormattedMessage id={'App.name'} />
                </Card.Title>
                <Card.Body>
                    <Stack gap={2} direction={'vertical'}>
                        <Button variant={"primary"} onClick={() => { LoginUser() }}>
                            <FormattedMessage id={'App.login.button'} />
                        </Button>
                    </Stack>
                </Card.Body>
            </Card>
            <Recaptcha />
        </div>
    )
}

export default Login;