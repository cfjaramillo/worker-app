import React from "react";
import { msalInstance } from "../../Utils/MsalClient";

function Logout() {

    React.useEffect(
        () => {
            LogoutUser()
        }
    );


    const logoutRequest = {
        mainWindowRedirectUri: "http://localhost:3000/",
    };


    function LogoutUser() {
        msalInstance.logoutPopup(logoutRequest).then(
            (res) => { console.log(res); }
        ).catch(
            (err) => { console.log(err); }
        ).finally(
            () => { window.location.href = "/"; }
        )
    }
}


export default Logout;