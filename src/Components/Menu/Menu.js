import { Nav, Navbar, Container } from "react-bootstrap";
import Logo from '../../Resources/img/logo.svg';
import { Link } from "react-router-dom";
import { FormattedMessage } from 'react-intl/';
import NavDropdown from 'react-bootstrap/NavDropdown';
import { BsSliders } from 'react-icons/bs';

const menuOptions = [
    { route: "/", name: "App.menuoptions.home", },
    { route: "/jobs", name: "App.menuoptions.jobs", },
    { route: "/offers", name: "App.menuoptions.offers" },
    { route: "/requests", name: "App.menuoptions.requests" },
]

const accountOptions = [
    { route: "/account", name: "App.menuoptions.account" },
    { route: "/logout", name: "App.menuoptions.logout" },
]

function Menu() {
    return (
        <Navbar collapseOnSelect bg={'dark'} variant={'dark'} expand={'md'}>
            <Navbar.Brand>
                <img src={Logo} width={'30'} height={'30'} className="d-inline-block align-top" alt={'logo'} />
                Worker APP
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            <Navbar.Collapse id="responsive-navbar-nav">
                <Nav className="me-auto">
                    {menuOptions.map(
                        (option) => {
                            return (
                                <Nav.Link key={option.route} as={'div'}>
                                    <Link to={option.route} >
                                        <FormattedMessage id={option.name} />
                                    </Link>
                                </Nav.Link>
                            )
                        }
                    )}
                </Nav>
                <Nav className="justify-content-end">
                    <NavDropdown id="collasible-nav-dropdown" title={<BsSliders />} menuVariant="dark" drop="start">
                        {accountOptions.map(
                            (option) => {
                                return (
                                    <NavDropdown.Item key={option.route} >
                                        <Link to={option.route} >
                                            <FormattedMessage id={option.name} />
                                        </Link>
                                    </NavDropdown.Item>
                                )
                            }
                        )}
                    </NavDropdown>
                </Nav>
                <Nav className="justify-content-end">
                    <Container fluid>
                        <span></span>
                    </Container>
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    )
}

export default Menu;