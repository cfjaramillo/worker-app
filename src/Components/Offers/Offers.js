import { FormattedMessage } from "react-intl";

function Offers() {
    return (
        <div className="page-title">
            <h1>
                <FormattedMessage id={'App.Offers.Label'} />
            </h1>
        </div>
    )
}


export default Offers;