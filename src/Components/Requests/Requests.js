import { FormattedMessage } from "react-intl";

function Requests() {
    return (
        <div className="page-title">
            <h1>
                <FormattedMessage id={'App.Requests.Label'} />
            </h1>
        </div>
    )
}

export default Requests;