import React from "react";
import { ToastContainer } from "react-bootstrap";
import Toast from 'react-bootstrap/Toast';
import { FormattedMessage } from "react-intl";

function ToastMessages({ children }) {

    const [show, setShow] = React.useState(true);

    return (
        <>
            <ToastContainer position={'top-end'}>
                <Toast show={show} onClose={() => setShow(!show)} delay={10000}>
                    <Toast.Header>
                        <FormattedMessage id="App.Toast.Header" />
                    </Toast.Header>
                    <Toast.Body>
                        {children}
                    </Toast.Body>
                </Toast>
            </ToastContainer>
        </>
    )
}

export default ToastMessages;