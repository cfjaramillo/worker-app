import React from "react";
import { msalInstance } from "../Utils/MsalClient";

export const UserAuthContext = React.createContext(msalInstance.getAllAccounts());