export const CatalogTypes = {
    IdentificationTypes: 1,
    WorkCategories: 2,
    StatusWork: 3,
    PaymentPeriod: 4
}

export const StatusTypes = {
    Created: 13,
    Progress: 14,
    Stopped: 15,
    Canceled: 16
}