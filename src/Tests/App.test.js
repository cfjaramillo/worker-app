import { render, screen } from '@testing-library/react';
import App from '../App';

test('renders login page and App title', () => {
  render(<App />);
  const language = navigator.language.substring(0, 2);
  let linkElement;

  switch (language) {
    case 'es':
      linkElement = screen.getByText(/Worker Español/i);
      break;
    case 'en':
      linkElement = screen.getByText(/Worker English/i);
      break;
    default:
      linkElement = screen.getByText(/Worker English/i);
      break;
  }

  expect(linkElement).toBeInTheDocument();
});


test('renders login button', () => {
  render(<App />);
  const language = navigator.language.substring(0, 2);
  let linkElement;

  switch (language) {
    case 'es':
      linkElement = screen.getByText(/Ingresar/i);
      break;
    case 'en':
      linkElement = screen.getByText(/Login/i);
      break;
    default:
      linkElement = screen.getByText(/Login/i);
      break;
  }

  expect(linkElement).toBeInTheDocument();
});
