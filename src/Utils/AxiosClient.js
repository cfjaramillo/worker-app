import axios from 'axios';

const baseURL = process.env.REACT_APP_WORKER_APIBASE_URI;

axios.defaults.headers.common['Authorization'] = `Bearer ${sessionStorage.getItem('workertoken')}`;

export const GetRequest = async (path) => {
    return axios.get(`${baseURL}${path}`);
}

export const PostRequest = async (path, data) => {
    return axios.post(`${baseURL}${path}`, data);
}