import * as msal from "@azure/msal-browser";

const msalConfig = {
    auth: {
        clientId: process.env.REACT_APP_AAD_WEBAPP_CLIENTID,
        authority: process.env.REACT_APP_AAD_WEBAPP_AUTHORITY,
        redirectUri: process.env.REACT_APP_ADD_WEBAPP_REDIRECTURI
    }
};

export const msalInstance = new msal.PublicClientApplication({
    auth: {
        clientId: msalConfig.auth.clientId,
        authority: msalConfig.auth.authority,
        redirectUri: msalConfig.auth.redirectUri
    },
    cache: {
        cacheLocation: 'sessionStorage',
        storeAuthStateInCookie: true
    }
});

export const account = msalInstance.getAllAccounts()[0];