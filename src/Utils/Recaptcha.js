import React from "react";
import { GoogleReCaptchaProvider, GoogleReCaptcha } from "react-google-recaptcha-v3";
import Loader from "../Components/Loader/Loader";

function Recaptcha() {

    const [token, setToken] = React.useState(null);
    const [isLoading, setLoading] = React.useState(true);


    async function onVerify(value) {
        if (token === null) {
            setToken(value);
            setLoading(false);
        }
        console.log(value);
    }

    return (
        <>
            {
                isLoading ? <Loader /> : null
            }

            <GoogleReCaptchaProvider
                reCaptchaKey={process.env.REACT_APP_WORKER_RECAPTCHA_SITE_KEY}
                scriptProps={{
                    async: true,
                    defer: true,
                    appendTo: 'head'
                }}
                container={{
                    parameters: {
                        theme: 'dark'
                    }
                }}>
                <GoogleReCaptcha onVerify={async (token) => { await onVerify(token) }}></GoogleReCaptcha>
            </GoogleReCaptchaProvider>
        </>
    )

}

export default Recaptcha