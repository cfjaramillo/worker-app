export const GetStatusColor = (id) => {

    let statusColor = {
        id: id
    };

    switch (id) {
        case 13:
            statusColor.color = 'primary'
            break;
        case 14:
            statusColor.color = 'success'
            break;
        case 15:
            statusColor.color = 'warning'
            break;
        case 16:
            statusColor.color = 'danger'
            break;
        default:
            statusColor.color = 'light'
            break;
    }

    return statusColor.color;

}